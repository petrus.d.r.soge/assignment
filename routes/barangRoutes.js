const express = require("express"); 
const router = express.Router(); 

const { imageUpload } = require("../middlewares/uploads/imageUpload");
const barangValidator = require("../middlewares/validators/barangValidator");

const barangController = require("../controllers/barangController");
const barang = require("../models/barang");

router.post("/", imageUpload, barangValidator.create, barangController.create);
router.get("/", barangController.getAll);
router.get("/:id", barangController.getOne);
router.put("/:id", imageUpload, barangValidator.create, barangController.update);
router.delete("/:id", barangController.delete);

module.exports = router; 
