const express = require("express");
const router = express.Router();

const pemasokController = require("../controllers/pemasokController");

router.post("/", pemasokController.create);
router.get("/", pemasokController.getAll);
router.get("/:id", pemasokController.getOne);
router.put("/:id", pemasokController.update);
router.delete("/:id", pemasokController.delete);

module.exports = router;